import React from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { answerQuestion, nextStep } from "@/store/slices/test-slice";
import {
  TextField,
  Button,
  FormControl,
  FormControlLabel,
  Checkbox,
  Radio,
  RadioGroup,
} from "@mui/material";
import { IQuestion } from "@/types";

interface QuestionProps {
  question: IQuestion;
}

export const Question: React.FC<QuestionProps> = ({ question }) => {
  const dispatch = useDispatch();

  const validationSchema = yup.object({
    answer: yup.mixed().required("Требуется ответ"),
  });

  const formik = useFormik({
    initialValues: {
      answer: question.type === "multiple" ? ([] as string[]) : "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      dispatch(
        answerQuestion({ questionId: question.id, answer: values.answer })
      );
      formik.resetForm();
      dispatch(nextStep());
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <h2>{question.text}</h2>
      {question.type === "short" || question.type === "long" ? (
        <TextField
          id="answer"
          name="answer"
          label="Ваш ответ"
          value={formik.values.answer}
          onChange={formik.handleChange}
          error={formik.touched.answer && Boolean(formik.errors.answer)}
          helperText={formik.touched.answer && formik.errors.answer}
          multiline={question.type === "long"}
          rows={question.type === "long" ? 4 : 1}
          fullWidth
        />
      ) : question.type === "single" ? (
        <FormControl component="fieldset">
          <RadioGroup
            aria-label="quiz"
            name="answer"
            value={formik.values.answer}
            onChange={formik.handleChange}
          >
            {question.options?.map((option, index) => (
              <FormControlLabel
                key={index}
                value={option}
                control={<Radio />}
                label={option}
              />
            ))}
          </RadioGroup>
        </FormControl>
      ) : question.type === "multiple" ? (
        <FormControl component="fieldset">
          {question.options?.map((option, index) => (
            <FormControlLabel
              key={index}
              control={
                <Checkbox
                  checked={formik.values.answer.includes(option)}
                  onChange={() => {
                    const newAnswers = Array.isArray(formik.values.answer)
                      ? formik.values.answer.includes(option)
                        ? formik.values.answer.filter(
                            (a: string) => a !== option
                          )
                        : [...formik.values.answer, option]
                      : formik.values.answer;
                    formik.setFieldValue("answer", newAnswers);
                  }}
                  name="answer"
                />
              }
              label={option}
            />
          ))}
        </FormControl>
      ) : null}
      <Button
        color="primary"
        variant="contained"
        fullWidth
        type="submit"
        disabled={!formik.dirty}
      >
        Отправить
      </Button>
    </form>
  );
};
