import { Step, StepLabel, Stepper } from "@mui/material";

interface IProgressProps {
  length: number;
  currentStep: number;
}

export const Progress: React.FC<IProgressProps> = ({ length, currentStep }) => {
  return (
    <Stepper activeStep={currentStep} alternativeLabel>
      {[...Array(length)].map((_, id) => (
        <Step key={id + 1}>
          <StepLabel>{id + 1}</StepLabel>
        </Step>
      ))}
    </Stepper>
  );
};
