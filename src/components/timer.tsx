import React, { useEffect, useState } from "react";
import { Typography, Box } from "@mui/material";
import { useDispatch } from "react-redux";
import { completeTest, setTimeLeft } from "@/store/slices/test-slice";
import { RootState } from "@/store/store";
import { useSelector } from "react-redux";
import { timeLeftSelector } from "@/store/selectors";

interface TimerProps {}

export const Timer: React.FC<TimerProps> = () => {
  const dispatch = useDispatch();
  const timeLeft = useSelector(timeLeftSelector);

  const handleSetTimeLeft = (timeLeft: number) => {
    dispatch(setTimeLeft(timeLeft));
  };

  const handleTimeUp = () => {
    dispatch(completeTest());
  };

  useEffect(() => {
    if (timeLeft === 0) {
      handleTimeUp();
      return;
    }

    const timerId = setInterval(() => {
      handleSetTimeLeft(timeLeft - 1);
    }, 1000);

    return () => clearInterval(timerId);
  }, [timeLeft, handleTimeUp]);

  const formatTime = (seconds: number) => {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${minutes}:${remainingSeconds < 10 ? "0" : ""}${remainingSeconds}`;
  };

  return (
    <Box>
      <Typography variant="h6" color="secondary" align="center">
        Оставшееся время: {formatTime(timeLeft)}
      </Typography>
    </Box>
  );
};
