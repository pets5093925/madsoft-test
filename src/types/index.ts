 export interface IQuestion {
  id: number;
  text: string;
  type: "single" | "multiple" | "short" | "long";
  options?: string[];
}

export interface IAnswer {
  questionId: number;
  answer: string | string[];
}
