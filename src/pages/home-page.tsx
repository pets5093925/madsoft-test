import React from "react";
import { Link } from "react-router-dom";
import { Button, Box, Typography } from "@mui/material";

const HomePage: React.FC = () => {
  return (
    <Box
      sx={{ width: "100%", height: "100%", display: "flex" }}
      alignContent={"center"}
      justifyContent={"center"}
    >
      <Box
        sx={{ display: "flex", width: "500px" }}
        justifyContent={"center"}
        flexDirection={"column"}
      >
        <Typography variant="h4" textAlign={"center"} sx={{ mb: 2 }}>
          Добро пожаловать в тестовую систему
        </Typography>
        <Button component={Link} variant="contained" color="primary" to="/test">
          Начать тест
        </Button>
      </Box>
    </Box>
  );
};

export default HomePage;
