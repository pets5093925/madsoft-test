import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Question } from "@/components/question";
import { Container, Box, Typography } from "@mui/material";
import { completeTest } from "@/store/slices/test-slice";
import { Timer } from "@/components/timer";
import { IQuestion } from "@/types";
import { Progress } from "@/components/progress";
import { completedSelector, currentStepSelector } from "@/store/selectors";

const mockTest: IQuestion[] = [
  {
    id: 1,
    text: "Какая столица Франции?",
    type: "single",
    options: ["Берлин", "Мадрид", "Париж", "Лиссабон"],
  },
  {
    id: 2,
    text: "Что из следующего является основными числами?",
    type: "multiple",
    options: ["2", "3", "4", "5"],
  },
  {
    id: 3,
    text: "Какая сумма 5 и 3?",
    type: "short",
  },
  {
    id: 4,
    text: "Опишите процесс фотосинтеза.",
    type: "long",
  },
  {
    id: 5,
    text: "Что из следующего является языком программирования?",
    type: "single",
    options: ["HTML", "CSS", "Python", "XML"],
  },
  {
    id: 6,
    text: "Выберите используемые вами библиотеки JavaScript.",
    type: "multiple",
    options: ["React", "Angular", "Vue", "jQuery"],
  },
  {
    id: 7,
    text: "Что такое квадратный корень из 64?",
    type: "short",
  },
  {
    id: 8,
    text: "Объясните значение периода эпохи Возрождения.",
    type: "long",
  },
];

const TestPage: React.FC = () => {
  const currentStep = useSelector(currentStepSelector);
  const isCompleted = useSelector(completedSelector);

  const dispatch = useDispatch();

  useEffect(() => {
    if (currentStep >= mockTest.length) {
      dispatch(completeTest());
    }
  }, [currentStep]);

  return (
    <Container maxWidth="md">
      <Box mt={5}>
        <Typography variant="h4" gutterBottom align="center">
          Тестирование
        </Typography>
        {!isCompleted && <Timer />}
        {isCompleted || currentStep >= mockTest.length ? (
          <Typography variant="h1" color="primary" align="center">
            Тест завершен
          </Typography>
        ) : (
          <>
            <Progress currentStep={currentStep} length={mockTest.length} />
            {mockTest
              .filter((question) => question.id === currentStep + 1)
              .map((question) => (
                <Question key={question.id} question={question} />
              ))}
          </>
        )}
      </Box>
    </Container>
  );
};

export default TestPage;
