import { createSelector } from "reselect";
import { RootState } from "@/store/store";

const testStateSelector = (state: RootState) => state.test;

export const timeLeftSelector = createSelector(
  [testStateSelector],
  (state) => state.timeLeft
);
export const currentStepSelector = createSelector(
  [testStateSelector],
  (state) => state.currentStep
);
export const answersSelector = createSelector(
  [testStateSelector],
  (state) => state.answers
);
export const completedSelector = createSelector(
  [testStateSelector],
  (state) => state.isCompleted
);
