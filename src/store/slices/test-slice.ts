import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IAnswer } from "../../types";
import { resetState } from "./reset-slice";

interface ITestState {
  timeLeft: number;
  currentStep: number;
  answers: IAnswer[];
  isCompleted: boolean;
}

const time = 600; // 10 minutes

const savedState = localStorage.getItem("test");

const initialState: ITestState = savedState
  ? JSON.parse(savedState)
  : {
      timeLeft: time,
      currentStep: 0,
      answers: [],
      isCompleted: false,
    };

const testSlice = createSlice({
  name: "test",
  initialState,
  reducers: {
    setTimeLeft(state, action: PayloadAction<number>) {
      state.timeLeft = action.payload;
    },
    answerQuestion(state, action: PayloadAction<IAnswer>) {
      state.answers.push(action.payload);
    },
    nextStep(state) {
      state.currentStep += 1;
    },
    prevStep(state) {
      state.currentStep -= 1;
    },
    completeTest(state) {
      state.isCompleted = true;
    },
  },
});

export const { answerQuestion, nextStep, prevStep, completeTest, setTimeLeft } =
  testSlice.actions;

export default testSlice.reducer;
