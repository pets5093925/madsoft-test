import { combineReducers, configureStore } from "@reduxjs/toolkit";
import testReducer from "./slices/test-slice";
import localStorageMiddleware from "./middleware/localStorageMiddleware";
import { resetState } from "./slices/reset-slice";

const appReducer = combineReducers({
  test: testReducer,
});

const rootReducer = (state: ReturnType<typeof appReducer>, action: any) => {
  if (action.type === resetState.type) {
    state = undefined;
  }
  return appReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(localStorageMiddleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
