import { Middleware, UnknownAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

const localStorageMiddleware: Middleware<{}, RootState> =
  (storeAPI) => (next) => (action: UnknownAction) => {
    const result = next(action);

    if (action.type.startsWith("test/") && action.type !== "test/resetState") {
      const state = storeAPI.getState();
      localStorage.setItem("test", JSON.stringify(state.test));
    }

    return result;
  };

export default localStorageMiddleware;
